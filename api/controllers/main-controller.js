import auth from "../helpers/auth";
import mysql from "mysql";

const pool = mysql.createPool({
  connectionLimit: 10,
  multipleStatements: true,
  host: "localhost",
  port: "8889",
  database: "KillerApp",
  user: "root",
  password: "root"
});

export function unprotectedGet(args, res, next) {
  let response = {
    message: "My resource!"
  };
  res.writeHead(200, {
    "Content-Type": "application/json"
  });
  return res.end(JSON.stringify(response));
}

export function protectedGet(args, res, next) {
  //*** YOUR ACTION CODE (db queries...) ***//
  let response = {
    message: "My protected resource for admins and users!"
  };
  res.writeHead(200, {
    "Content-Type": "application/json"
  });
  return res.end(JSON.stringify(response));
}

// =================  GAMES ========================= //

export function getGames(args, res, next) {
  pool.query(
    `SELECT games.*, GROUP_CONCAT(isPlayingIn.userId) as players, users.userId as creator, users.username as creatorName FROM games LEFT JOIN isPlayingIn ON games.gameId = isPlayingIn.inGameId LEFT JOIN users ON users.userId = games.authorId GROUP BY games.gameId
              `,
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      result.forEach(game => {
        if (typeof game.players === "string") {
          game.players = game.players.split(",").map(Number);
        }
      });
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    }
  );
}

export function getUsersInGame(args, res, next) {
  const value = args.swagger.params.gameId.value;
  // let array = new Array()
  // const id = value.length > 1 ? array = : array.push(value)
  // console.log(id)
  // id.map(id => `'${id}'`)
  pool.query(
    `SELECT * FROM isPlayingIn LEFT JOIN (SELECT userId, username, kills FROM users) as users ON users.userId= isPlayingIn.userId WHERE inGameId = '${value}' `,
    function (err, result, fields) {
      // `SELECT games.*, GROUP_CONCAT(isPlayingIn.userId) as players, users.userId as creator, users.username as creatorName FROM games LEFT JOIN isPlayingIn ON games.gameId = isPlayingIn.inGameId LEFT JOIN users ON users.userId = games.authorId WHERE gameId = '${value}' GROUP BY games.gameId`
      if (err) {
        console.log(err.code);
      }
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    }
  );
}

export function createGame(args, res, next) {
  //TODO: validation and sanitization
  const {
    userId,
    name,
    gameType,
    minPlayers,
    maxPlayers,
    gameDate,
    gameDuration
  } = args.body.data;
  pool.query(
    `INSERT INTO games (name, type, minPlayers, maxPlayers, authorId)
    VALUES ('${name}','${gameType}','${minPlayers}','${maxPlayers}','${userId}')`,
    // `INSERT INTO games (name, gameType, minPlayers, maxPlayers, gameDate)
    // VALUES ('${name}','${gameType}','${minPlayers}','${maxPlayers}','${gameDate}')`,
    // INSERT INTO isPlayingIn  (userId, inGameId) VALUES ('${userId}','${gameId}')
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      console.log(result.insertId);
      pool.query(
        `INSERT INTO isPlayingIn (userId, inGameId) VALUES ('${userId}','${
          result.insertId
        }')`,
        function (err, result, fields) {
          res.writeHead(200, {
            "Content-Type": "application/json"
          });
          return res.end(JSON.stringify(result));
        }
      );
    }
  );
}

export function startGame(args, res, next) {
  //TODO: validation and sanitization
  const {
    gameId
  } = args.body.data;
  pool.query(
    `UPDATE games
    SET gameStatus = 2
    WHERE gameId = ${gameId};
    SELECT * FROM isPlayingIn 
    LEFT JOIN (SELECT userId, username, kills FROM users) as users ON users.userId= isPlayingIn.userId 
    WHERE inGameId = ${gameId}`,
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      const killerId = result[1][Math.floor(Math.random() * result[1].length)].userId;
      const players = result[1].filter(item => item.userId !== killerId);
      const spyId = players[Math.floor(Math.random() * players.length)].userId;
      const rest = players
        .filter(item => item.userId !== spyId)
        .map(item => item.userId);
      console.log(killerId, spyId, rest);

      pool.query(`
      UPDATE isPlayingIn
      SET playerStatus = 1
      WHERE inGameId = ${gameId};
      UPDATE isPlayingIn
      SET playerStatus = 2
      WHERE inGameId = ${gameId} AND userId = ${killerId};
      UPDATE isPlayingIn
      SET playerStatus = 3
      WHERE inGameId = ${gameId} AND userId = ${spyId};
      `, function (err, result, fields) {
        if (err) {
          console.log(err.code);
        }
        res.writeHead(200, {
          "Content-Type": "application/json"
        });
        return res.end(JSON.stringify(result));
      })
    }
  );
}

export function updatePlayersInGame(args, res, next) {
  //TODO: validation and sanitization
  const {
    gameId,
    scannedPlayerId,
    scanningPlayerId
  } = args.body.data;
  // const scannedPlayerId = 5
  // const scanningPlayerId = 10
  console.log(gameId, scannedPlayerId, scanningPlayerId)
  pool.query(`
  SELECT * FROM isPlayingIn 
    LEFT JOIN (SELECT userId, username, kills FROM users) as users ON users.userId= isPlayingIn.userId 
    WHERE inGameId = ${gameId}
  `, function (err, result, fields) {
    if (err) {
      console.log(err.code);
    }

    const p1 = result.filter(item => item.userId === scannedPlayerId)[0]
    const p1st = p1.playerStatus
    const p2 = result.filter(item => item.userId === scanningPlayerId)[0]
    const p2st = p2.playerStatus

    const preys = result.filter(item => item.userId !== p1.userId && item.userId !== p2.userId && item.playerStatus === 1)
    console.log(p1.userId, p2.userId, preys)
    const randomPrey = preys[Math.floor(Math.random() * preys.length)].userId
    console.log(randomPrey)

    let p1newSt = p1.playerStatus
    let p1kills = p1.gameKills
    let p2newSt = p2.playerStatus
    let p2kills = p2.gameKills
    let addRandomPrey = false

    console.log(p1newSt, p1kills, p2newSt, p2kills)

    switch (true) {
      case p1st === 1 && p2st === 1:
        console.log('nothing happens')
        break;
      case p1st === 1 && p2st === 2:
        console.log('A change status to 4 / B add 1 kill')
        p1newSt = 4
        p2kills += 1
        break;
      case p1st === 1 && p2st === 3:
        console.log('A change status to 3, add 1 kill / B change status to 4')
        p1newSt = 3
        p1kills += 1
        p2newSt = 4
        break;
      case p1st === 2 && p2st === 1:
        console.log('A add 1 kill / B change status to 4')
        p1kills += 1
        p2newSt = 4
        break;
      case p1st === 2 && p2st === 3:
        console.log('A change status to 4 / B add 1 kill, change status to 2, if (prey amount > 1) then random prey becomes 3')
        p1newSt = 4
        p2kills += 1
        p2newSt = 2
        if (preys.length > 1) {
          addRandomPrey = true
        }
        break;
      case p1st === 3 && p2st === 1:
        console.log('A change status to 4 / B add 1 kill, B change status to 3')
        p1newSt = 4
        p2kills += 1
        p2newSt = 3
        break;
      case p1st === 3 && p2st === 2:
        console.log('A add 1 kill, change status to 2, if (prey amount > 1) then random prey becomes 3 / B change status to 4')
        p1kills += 1
        p1newSt = 2
        if (preys.length > 1) {
          addRandomPrey = true
        }
        p2newSt = 4
        break;
    }

    console.log(p1newSt, p1kills, p2newSt, p2kills)

    pool.query(`
    UPDATE isPlayingIn
    SET playerStatus = ${p1newSt}, gameKills = ${p1kills}
    WHERE inGameId = ${gameId} AND userId = ${p1.userId};
    UPDATE isPlayingIn
    SET playerStatus = ${p2newSt}, gameKills = ${p2kills}
    WHERE inGameId = ${gameId} AND userId = ${p2.userId};
    `, function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    })

  })
}

export function addPlayerToGame(args, res, next) {
  //TODO: validation and sanitization
  const {
    userId,
    gameId
  } = args.body.data;
  pool.query(
    `INSERT INTO isPlayingIn (userId, inGameId)
    VALUES ('${userId}','${gameId}')`,
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    }
  );
}

export function deleteGame(args, res, next) {
  const value = args.swagger.params.gameId.value;
  console.log(value);
  pool.query(
    `DELETE FROM isPLayingIn WHERE inGameId = ${value};
    DELETE FROM games WHERE gameId = ${value}`,
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    }
  );
}

// =================  USERS ========================= //

export function getUsers(args, res, next) {
  pool.query("SELECT * FROM users", function (err, result, fields) {
    if (err) {
      console.log(err.code);
    }
    result.forEach(user => {
      delete user.password;
    });
    res.writeHead(200, {
      "Content-Type": "application/json"
    });
    return res.end(JSON.stringify(result));
  });
}

export function getHof(args, res, next) {
  // ASC or DESC to change direction of ordering
  pool.query(
    "SELECT username, kills FROM users ORDER BY kills DESC LIMIT 10",
    function (err, result, fields) {
      if (err) {
        console.log(err.code);
      }
      result.forEach(user => {
        delete user.password;
      });
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    }
  );
}

export function getUserById(args, res, next) {
  const value = args.swagger.params.userId.value;
  // let array = new Array()
  // const id = value.length > 1 ? array = : array.push(value)
  // console.log(id)
  // id.map(id => `'${id}'`)
  pool.query(`SELECT * FROM users WHERE userId IN (${value})`, function (
    err,
    result,
    fields
  ) {
    if (err) {
      console.log(err.code);
    }
    if (result) {
      result.forEach(user => {
        delete user.password;
      });
    }
    res.writeHead(200, {
      "Content-Type": "application/json"
    });
    return res.end(JSON.stringify(result));
  });
}

export function validateUser(args, res, next) {
  console.log(args.body.data)
  const {
    username,
    password,
    email
  } = args.body.data;
  pool.query(`SELECT * FROM users WHERE username = '${username}';
  SELECT * FROM users WHERE email ='${email}' `, function (
    err,
    result,
    fields
  ) {
    if (err) {
      console.log(err.code);
    }
    res.writeHead(200, {
      "Content-Type": "application/json"
    });
    return res.end(JSON.stringify(result));
  });
}

export function createUser(args, res, next) {
  //TODO: validation and sanitization
  const {
    username,
    password,
    email
  } = args.body.data;
  console.log(username, password, email)
  pool.query(
    `INSERT INTO users (username, password, email) VALUES ('${username}','${password}','${email}')`,
    function (err, result, fields) {
      if (err) {
        console.log(err)
      }
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(result));
    })
}

export function loginPost(args, res, next) {
  const {
    username,
    password
  } = args.body.data;
  pool.query(`SELECT * FROM users WHERE username = '${username}'`, function (
    err,
    result,
    fields
  ) {
    const dbUser = result[0]
    if (dbUser !== undefined && username == dbUser.username && password === dbUser.password) {
      let tokenString = auth.issueToken(username);
      let response = {
        token: tokenString,
        userId: dbUser.userId
      };
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(response));
    } else {
      let response = {
        message: "Error: Credentials incorrect"
      };
      res.writeHead(403, {
        "Content-Type": "application/json"
      });
      return res.end(JSON.stringify(response));
    }
  })
}