"use strict";

var jwt = require("jsonwebtoken");
const JWT_SECRET_KEY = "shh";
const issuer = "my-awesome-website.com";

//Here we setup the security checks for the endpoints
//that need it (in our case, only /protected). This
//function will be called every time a request to a protected
//endpoint is received
exports.verifyToken = function(req, authOrSecDef, token, callback) {
  //these are the scopes/roles defined for the current endpoint

  function sendError() {
    return req.res.status(403).json({ message: "Error: Access Denied" });
  }

  //validate the 'Authorization' header. it should have the following format:
  //'Bearer tokenString'
  if (token && token.indexOf("Bearer ") == 0) {
    let tokenString = token.split(" ")[1];

    jwt.verify(tokenString, JWT_SECRET_KEY, function(
      verificationError,
      decodedToken
    ) {
      //check if the JWT was verified correctly
      if (verificationError == null && decodedToken) {
        // check if the issuer matches
        let issuerMatch = decodedToken.iss == issuer;

        // you can add more verification checks for the
        // token here if necessary, such as checking if
        // the username belongs to an active user

        if (issuerMatch) {
          //add the token to the request so that we
          //can access it in the endpoint code if necessary
          req.auth = decodedToken;
          //if there is no error, just return null in the callback
          return callback(null);
        } else {
          //return the error in the callback if there is one
          return callback(sendError());
        }
      } else {
        //return the error in the callback if the JWT was not verified
        return callback(sendError());
      }
    });
  } else {
    //return the error in the callback if the Authorization header doesn't have the correct format
    return callback(sendError());
  }
};

exports.issueToken = function(username) {
  let token = jwt.sign(
    {
      sub: username,
      iss: issuer
    },
    JWT_SECRET_KEY
  );
  return token;
};
