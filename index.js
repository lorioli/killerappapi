"use strict";

import express from "express";
import swaggerTools from "swagger-tools";
import YAML from "yamljs";
import auth from "./api/helpers/auth";
import cors from "cors"

const app = express();

app.use(cors())
/*
var app = require("express")();
var swaggerTools = require("swagger-tools");
var YAML = require("yamljs");
var auth = require("./api/helpers/auth");
*/
const swaggerConfig = YAML.load("./api/swagger/swagger.yaml");

swaggerTools.initializeMiddleware(swaggerConfig, function (middleware) {
  //Serves the Swagger UI on /docs
  app.use(middleware.swaggerMetadata()); // needs to go BEFORE swaggerSecurity

  app.use(
    middleware.swaggerSecurity({
      //manage token function in the 'auth' module
      Bearer: auth.verifyToken
    })
  );

  const routerConfig = {
    controllers: "./api/controllers",
    useStubs: false
  };

  app.use(middleware.swaggerRouter(routerConfig));

  app.use(middleware.swaggerUi());

  app.listen(3000, function () {
    console.log("Started server on port 3000");
  });
});